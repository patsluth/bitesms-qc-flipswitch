THEOS_PACKAGE_DIR_NAME = debs
THEOS_DEVICE_IP = 127.0.0.1
THEOS_DEVICE_PORT = 2222
ARCHS = armv7 armv7s arm64
TARGET = iphone:clang:latest:7.0

BUNDLE_NAME = BiteSMSQCFlipswitch
BiteSMSQCFlipswitch_CFLAGS = -fobjc-arc
BiteSMSQCFlipswitch_FILES = Switch.xm
BiteSMSQCFlipswitch_FRAMEWORKS = UIKit
BiteSMSQCFlipswitch_LIBRARIES = flipswitch sw
BiteSMSQCFlipswitch_INSTALL_PATH = /Library/Switches

include theos/makefiles/common.mk
include $(THEOS_MAKE_PATH)/tweak.mk
include $(THEOS_MAKE_PATH)/bundle.mk
include $(THEOS_MAKE_PATH)/library.mk

after-install:: @install.exec "killall -9 SpringBoard"