#import <libsw/SWAppLauncher.h>

#import <Springboard/Springboard.h>

#import "FSSwitchDataSource.h"
#import "FSSwitchPanel.h"

#import <objc/runtime.h>

@interface BiteSMSQCFlipswitchSwitch : NSObject
{
}
@end

@implementation BiteSMSQCFlipswitchSwitch

- (FSSwitchState)stateForSwitchIdentifier:(NSString *)switchIdentifier
{
	return FSSwitchStateOff;
}

- (void)applyState:(FSSwitchState)newState forSwitchIdentifier:(NSString *)switchIdentifier
{
    SEL sel = NSSelectorFromString(@"showQuickCompose");
    id bite = objc_getClass("BSQCQRLauncher");

    if (bite && [bite respondsToSelector:sel]){
        [bite performSelectorOnMainThread:sel withObject:nil waitUntilDone:NO];
    }
}

- (void)applyAlternateActionForSwitchIdentifier:(NSString *)switchIdentifier
{
    SBApplicationController *apc = [%c(SBApplicationController) sharedInstanceIfExists];

    if (apc){
        [SWAppLauncher launchAppLockscreenFriendly:[apc applicationWithDisplayIdentifier:@"com.apple.MobileSMS"]];
    }
}

@end




